import React, { useEffect, useState } from 'react';
import { useHistory } from 'react-router';
import './Nav.scss';

function Nav(props) {
	const [show, setShow] = useState(false);
	const history = useHistory();

	useEffect(() => {
		const transitionNavbar = () => {
			if (window.scrollY > 200) {
				setShow(true);
			} else {
				setShow(false);
			}
		};
		window.addEventListener('scroll', transitionNavbar);
		return () => window.removeEventListener('scroll', transitionNavbar);
	}, []);

	return (
		<div className={`nav ${show && 'nav-black'}`}>
			<div className='nav__content'>
				<img
					className='nav__logo'
					src='http://assets.stickpng.com/images/580b57fcd9996e24bc43c529.png'
					alt=''
				/>
				<img
					onClick={() => history.push('/profile')}
					className='nav__avatar'
					src='https://mir-s3-cdn-cf.behance.net/project_modules/disp/84c20033850498.56ba69ac290ea.png'
					alt=''
				/>
			</div>
		</div>
	);
}

export default Nav;
