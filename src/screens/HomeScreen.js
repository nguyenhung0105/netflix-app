import React from 'react';
import Banner from '../Banner';
import './HomeScreen.scss';
import Nav from '../Nav';
import Row from '../Row';
import requests from '../APITMBD/Requests';

function HomeScreen(props) {
	return (
		<div className='home-screen'>
			<Nav />
			<Banner />
			<Row
				title='NETFLIX ORIGINAL'
				fetchUrl={requests.fetchNetflixOriginals}
				isLargeRow
			/>
			<Row title='Trending Now' fetchUrl={requests.fetchTrending} />
			<Row title='Top Rate' fetchUrl={requests.fetchTopRated} />
			<Row title='Action Movies' fetchUrl={requests.fetchActionMovies} />
			<Row title='Comedy Movies' fetchUrl={requests.fetchNetflixOriginals} />
			<Row title='Horror Movies' fetchUrl={requests.fetchHorrorMovies} />
			<Row title='Romance Movies' fetchUrl={requests.fetchRomanceMovies} />
			<Row
				title='Documentaries Movies'
				fetchUrl={requests.fetchDocumentaries}
			/>
		</div>
	);
}

export default HomeScreen;
