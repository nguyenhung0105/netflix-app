const API_KEY = '8b3387fab22eaf8ef0c8bfefd7057711';

const requests = {
	fetchTrending: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=1`,
	fetchNetflixOriginals: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=2`,
	fetchTopRated: `/discover/tv/?api_key=${API_KEY}&with_networks=3`,
	fetchActionMovies: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=4`,
	fetchComedyMovies: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=10`,
	fetchHorrorMovies: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=41`,
	fetchRomanceMovies: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=20`,
	fetchDocumentaries: `/discover/movie?sort_by=popularity.desc&api_key=${API_KEY}&page=99`,
};

export default requests;

// https://api.themoviedb.org/3/discover/tv/?api_key=bf44f525408d80193f5c831e32139a87&with_networks=213`

// bf44f525408d80193f5c831e32139a87

//https://api.themoviedb.org/3/discover/movie?sort_by=popularity.desc&api_key=8b3387fab22eaf8ef0c8bfefd7057711&page=3
