import firebase from 'firebase';

const firebaseConfig = {
	apiKey: 'AIzaSyA3Jzyns5ixmlSjStXJXrM0bbfcx21D57g',
	authDomain: 'netflix-app-d249d.firebaseapp.com',
	projectId: 'netflix-app-d249d',
	storageBucket: 'netflix-app-d249d.appspot.com',
	messagingSenderId: '429478774676',
	appId: '1:429478774676:web:fe480b20dc8d3f9d5fb181',
};

const firebaseApp = firebase.initializeApp(firebaseConfig);
const db = firebaseApp.firestore();
const auth = firebase.auth();

export { auth };
export default db;
